//
//  User.swift
//  LaboLogin
//
//  Created by anh.buiphuong on 10/5/18.
//  Copyright © 2018 huy.thamgia. All rights reserved.
//

import Foundation
import SwiftyJSON
class UserProfile: NSObject, NSCoding{
    
    var userId: String?
    var role: String?
    var name: String?
    init(json: JSON) {
        role = json["role"].stringValue
        userId = json["userId"].stringValue
        name = json["userName"].stringValue
    }
    init(userId:String, role:String, name:String){
        self.userId = userId
        self.role = role
        self.name = name
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(userId, forKey: "userId")
        aCoder.encode(role, forKey: "role")
        aCoder.encode(name, forKey: "name")
    }
   
    required convenience init?(coder aDecoder: NSCoder) {
        let userId = aDecoder.decodeObject(forKey: "userId") as! String
        let role = aDecoder.decodeObject(forKey: "role") as! String
        let name = aDecoder.decodeObject(forKey: "name") as! String
        self.init(userId: userId, role: role, name: name)
    }
    
    
}
