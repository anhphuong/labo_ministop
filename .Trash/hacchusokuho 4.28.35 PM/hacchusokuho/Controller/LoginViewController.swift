//
//  ViewController.swift
//  LaboLogin
//
//  Created by huy.thamgia on 10/3/18.
//  Copyright © 2018 huy.thamgia. All rights reserved.
//

import UIKit
typealias DICT = Dictionary<AnyHashable, Any>

class LoginViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var userTextField: CustomTextField!
    @IBOutlet weak var passTextField: CustomTextField!
    @IBOutlet weak var loginButton: CustomButton!
    let loginServices: LoginServices = LoginServices()
    var user: UserProfile?
    var homeController : HomeViewController?
    private enum checkLogin {
        case userAndPassWrong
        case userAndPassEmpty
        case noEmployee
        case success
    }
    override func viewDidLoad() {
        super.viewDidLoad()
<<<<<<< HEAD
=======
        loadingIndicator.isHidden = true
        print(AccessVariables.getDomainApi("domain api")!)
>>>>>>> 0d2989093b282fe1f0bad7ec32b43a77432d1bcb
    }
    override func viewDidAppear(_ animated: Bool) {
        //check for user profile if they haven't logout before
        if let user = loginServices.checkUserProfile() {
            self.user = user
            loggedIn()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    private func checkUserLogin() -> checkLogin{
        if userTextField.text == passTextField.text {
            let first = userTextField.text?.prefix(1)
            //When userTextField and passTextField not empty
            if userTextField.text != "" || passTextField.text != ""{
                //When userTextField and passTextField first character = "g"
                if first == "g" {
                    return .success
                } else {
<<<<<<< HEAD
                   return .userAndPassWrong
                }
            } else {
                return .userAndPassEmpty
            }
        } else {

            return .userAndPassWrong
        }
    }
    //Login Button pressed
    @IBAction private  func loginButton(_ sender: UIButton) {
        switch checkUserLogin() {
        case .userAndPassWrong:
            Alert.show(from: self, title: "Alert", message: Message.userAndPassWrong, okTitle: "Back")
        case .userAndPassEmpty:
            Alert.show(from: self, title: "Alert", message: Message.userAndPassEmpty, okTitle: "Back")
        case .noEmployee:
            Alert.show(from: self, title: "Alert", message: Message.noEmployee, okTitle: "Back")
      
        default:
            doLogin()
            
        }
        
    }
=======
                    //Show message ユーザーIDとパスワードが間違っています。
                    Alert.show(from: self, title: "通知", message: String.userAndPassWrong, okTitle: "Back", okHandle: {
                        self.userTextField.becomeFirstResponder()
                    })
                }
            } else {
                //Show message ユーザーIDとパスワードを入力してください。
                    Alert.show(from: self, title: "通知", message: String.userAndPassEmpty, okTitle: "Back", okHandle: {
                        self.userTextField.becomeFirstResponder()
                    })
            }
        } else {
            //Show message ユーザーIDとパスワードが間違っています。
            Alert.show(from: self, title: "通知", message: String.userAndPassWrong, okTitle: "Back", okHandle: {
                self.userTextField.becomeFirstResponder()
            })
        }
    }
    
>>>>>>> 0d2989093b282fe1f0bad7ec32b43a77432d1bcb
    //Add 'done' button to hide keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        userTextField.resignFirstResponder()
        passTextField.resignFirstResponder()
        return true
    }
    
}
extension LoginViewController {
    
    fileprivate func doLogin() {
        //start loading animation
        //remove first character
        let nameUser = String(userTextField.text!.dropFirst())
        let namePass = String(passTextField.text!.dropFirst())
        
        //Check if userCode exist
        Popup.showProgress()
        loginServices.getUserProfile(userName: nameUser, userPassword: namePass, sucess: { [unowned self] (user) in
            Popup.dismissProgress()
            self.user = user
            self.loginServices.saveUserProfile(user: user)
            self.loggedIn()
          
        }) { [unowned self] in
            //Show message 社員が存在しない。
            Popup.dismissProgress()
            Alert.show(from: self, title: "Alert", message: Message.noEmployee, okTitle: "Back", okHandle: {
                self.userTextField.becomeFirstResponder()
            })
        }
    }
    
    fileprivate func loggedIn() {
        //jump to HomeViewController
        let nextViewController = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        nextViewController.userProfile = user
        self.present(nextViewController, animated: true, completion: nil)
    }
}


