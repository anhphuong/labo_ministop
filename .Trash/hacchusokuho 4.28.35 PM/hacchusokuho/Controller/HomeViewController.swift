//
//  MainViewController.swift
//  LaboLogin
//
//  Created by huy.thamgia on 10/3/18.
//  Copyright © 2018 huy.thamgia. All rights reserved.
//

import UIKit
import SVProgressHUD
class HomeViewController: UIViewController {
    @IBOutlet private weak var roleLabel: UILabel!
    @IBOutlet private weak var idLabel: UILabel!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var notificationLabel: UILabel!
    var userProfile: UserProfile?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchUserProfile()
        
    }
    
    func fetchUserProfile() {
        if let user = userProfile {
            idLabel.text = user.userId
            roleLabel.text = user.role
            nameLabel.text = user.name
            if user.role == "SACode" {
                notificationLabel.text = "740f4707 bebcf74f 9b7c25d4 8e335894 5f6aa01d a5ddb387 462c7eaf 61bb78ad"
            } else {
                notificationLabel.isHidden = true
            }
        }
    }

   @objc @IBAction private func handleLogout() {
        UserDefaults.standard.removeObject(forKey: "user")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
