//
//  Alert.swift
//  LaboLogin
//
//  Created by huy.thamgia on 10/5/18.
//  Copyright © 2018 huy.thamgia. All rights reserved.
//

import UIKit

class Alert {
    static func show(from controller: UIViewController,
                     title: String,
                     message: String,
                     okTitle: String,
                     okHandle: (() -> ())? = nil,
                     cancleTitle: String? = nil,
                     cancleHandle: (() -> ())? = nil) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: okTitle, style: .default, handler: { (_) in
            okHandle?()
        })
        alert.addAction(okAction)
        
        if let cancleTitle = cancleTitle {
            let cancelAction = UIAlertAction(title: cancleTitle, style: .default, handler: { (_) in
                cancleHandle?()
            })
            alert.addAction(cancelAction)
        }
        
        
        controller.present(alert, animated: true, completion: nil)
    }
}
