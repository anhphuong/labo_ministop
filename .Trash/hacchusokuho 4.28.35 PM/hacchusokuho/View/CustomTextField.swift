//
//  CustomTextField.swift
//  LaboLogin
//
//  Created by huy.thamgia on 10/3/18.
//  Copyright © 2018 huy.thamgia. All rights reserved.
//

import UIKit

//Round up textfieldmmm
class CustomTextField: UITextField {
    private var _cornerRadius: CGFloat = 0.0
    @IBInspectable
    var cornerRadius: CGFloat {
        set (newValue) {
            _cornerRadius = newValue
            layer.cornerRadius = _cornerRadius
            layer.borderWidth = 2
            layer.masksToBounds = true
            layer.borderColor = UIColor.white.cgColor
        } get {
            return _cornerRadius
        }
    }
    
}
