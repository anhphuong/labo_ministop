//
//  LoginManagement.swift
//  hacchusokuho
//
//  Created by huy.thamgia on 10/11/18.
//  Copyright © 2018 huy.thamgia. All rights reserved.
//

import Foundation

extension LoginViewController {
    
    func doLogin() {
        //start loading animation
        startLoading()
        
        //remove first character
        let nameUser = String(userTextField.text!.dropFirst())
        let namePass = String(passTextField.text!.dropFirst())
        
        //Check if userCode exist
        loginServices.getUserProfile(userName: nameUser, userPassword: namePass, sucess: { (user) in
            self.user = user
            self.saveUserProfile(user: user)
            self.loggedIn()
        }) {
            //Show message 社員が存在しない。
            Alert.show(from: self, title: "通知", message: String.noEmployee, okTitle: "Back", okHandle: {
                self.stopLoading()
                self.userTextField.becomeFirstResponder()
            })
        }
    }
    
    func loggedIn() {
        //start loading animation
        stopLoading()
        
        //jump to HomeViewController
        let nextViewController = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        nextViewController.userProfile = user
        self.present(nextViewController, animated: true, completion: nil)
    }
}
