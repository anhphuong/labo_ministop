//
//  message.swift
//  hacchusokuho
//
//  Created by huy.thamgia on 10/11/18.
//  Copyright © 2018 huy.thamgia. All rights reserved.
//

import Foundation

struct Message{
    public static let userAndPassWrong = "ユーザーIDとパスワードが間違っています。"
    
    public static let userAndPassEmpty = "ユーザーIDとパスワードを入力してください。"
    
    public static let noEmployee = "社員が存在しない。"
}
