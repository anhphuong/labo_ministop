//
//  Popup.swift
//  hacchusokuho
//
//  Created by anh.buiphuong on 10/12/18.
//  Copyright © 2018 huy.thamgia. All rights reserved.
//

import Foundation
import SVProgressHUD
class Popup {
    static func showProgress() {
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.show()
        
    }
    static func dismissProgress() {
        SVProgressHUD.dismiss()
    }
}
