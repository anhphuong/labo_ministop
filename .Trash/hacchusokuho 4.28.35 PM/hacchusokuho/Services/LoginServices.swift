//
//  LoginServices.swift
//  LaboLogin
//
//  Created by anh.buiphuong on 10/5/18.
//  Copyright © 2018 huy.thamgia. All rights reserved.
//

import UIKit
class LoginServices {
    private var api: API!
    public func getUserProfile(userName: String, userPassword:String,  sucess: ((UserProfile) -> ())?, fail: (() -> ())?) {
        
        let parameters = [
            "userCode": userName,
            "passWord": userPassword,
            "registrationNoti": "740f4707 bebcf74f 9b7c25d4 8e335894 5f6aa01d a5ddb387 462c7eaf 61bb78ad"]
        api = API(endpoint: API.endpointType.login)
        api.call(parameters: parameters, fail: { _ in
            fail?()
        }){json in
            let code = json["code"].intValue
            if code == 1 {
                let user = UserProfile(json: json)
                sucess!(user)
            }
            else {
               
                fail!()
            }
            
        }
    }
    func saveUserProfile(user : UserProfile) {
        let userDefaults = UserDefaults.standard
        let encodeData: Data = NSKeyedArchiver.archivedData(withRootObject: user)
        print(encodeData)
        userDefaults.set(encodeData, forKey: "user")
    }
    
    func checkUserProfile() -> UserProfile?  {
        if let decoded = UserDefaults.standard.object(forKey: "user") as? Data {
            let userProfile = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! UserProfile
            return userProfile
        } else {
            return nil 
        }
    }
}
