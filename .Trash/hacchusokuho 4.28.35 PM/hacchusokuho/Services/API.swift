//
//  API.swift
//  LaboLogin
//
//  Created by anh.buiphuong on 10/5/18.
//  Copyright © 2018 huy.thamgia. All rights reserved.
//


import SwiftyJSON
import UIKit
import Alamofire

class API {
    public enum endpointType: String {
        
        case login
        
        func getValue() -> String {
            switch self {
            case .login:
                return AccessVariables.getApiPath("login")
            }
            
        }
        
        func method() -> HTTPMethod {
            switch self {
            case .login:
                return .post
            }
        }
    }
    
    var endpoint: String = ""
    var method: HTTPMethod = .get
    var manager: SessionManager!
    private var headerCommon: [String: String] = ["Content-Type":"application/json"]
    let printResponse: Bool = true
    
    static var path: String = AccessVariables.getDomainApi("domain api")!
    
    static var timeout: Double = 60
    
    init(){
        self.createSessionManager()
    }
    
    init(endpoint: API.endpointType) {
        self.endpoint = endpoint.getValue()
        self.method = endpoint.method()
        self.createSessionManager()
    }
    init(endpoint: String, method: HTTPMethod = .get) {
        self.endpoint = endpoint
        self.method = method
        self.createSessionManager()
    }
    
    private func createSessionManager() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = TimeInterval(API.timeout) // seconds
        configuration.timeoutIntervalForResource = TimeInterval(API.timeout)
        self.manager = SessionManager(configuration: configuration)
    }
    
    public func call(parameters: [String : Any], isYoutubeAPI: Bool = false, headersAdditional: [String : String]? = nil, fail: @escaping (_ error: JSON) -> (), success: @escaping (_ data: JSON) -> ()) {
        self.networkOperation(method: self.method, parameters: parameters, headersAdditional: headersAdditional, fail: fail, success: success)
    }
    
    private func networkOperation(method useMethod: HTTPMethod, parameters: [String : Any], headersAdditional: [String : String]? = nil, isYoutubeAPI: Bool = false, fail: @escaping (_ error: JSON) -> (), success: @escaping (_ data: JSON) -> ()) {
        
        var headers = headerCommon
        
        if headersAdditional != nil {
            for (k, v) in headersAdditional! {
                headers[k] = v
            }
        }
        
        var url = API.path + endpoint
        if endpoint.hasPrefix("http") {
            url = endpoint
        }
        
        var params: [String:Any]?

        if useMethod == .get {
            if parameters.count > 0 {
                var paramsToAdd: [String : Any] = [:]
                for (key, name) in parameters {
                    paramsToAdd[key] = name
                }
                if paramsToAdd.count > 0 {
                    var apiParams = ""
                    for (key, name) in paramsToAdd {
                        apiParams = apiParams + key + "=" + "\(name)" + "&"
                    }
                    if apiParams.hasSuffix("&") {
                        apiParams = String(apiParams.dropLast())
                    }
                    url = "\(url)?\(apiParams)"
                }

                params = paramsToAdd
            }
        } else {
            params = parameters
        }
        
        guard let urlEncoded = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            self.handleFailResponse(response: nil, data: nil, dictionary: { (json) in
                fail(json)
            })
            return
        }
        if self.printResponse {
            print("________________________________________________________")
            print("url:   \(urlEncoded)")
            print("headers:   \(headers)")
            print("params:   \(parameters)")
        }
       // create data
        var data = """
                {
            """
        if parameters.count > 0 {
            for (key, name) in parameters {
                data = data + "\"\(key)\":" + "\"\(name)\","
            }
            if data.hasSuffix(",") {
                data = String(data.dropLast())
            }
             data = data + "}"
        }
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = data.data(using: .utf8)
        
        self.manager.request(request as URLRequestConvertible).responseJSON { (response) in
            
            print("________________________________________________________")
            if let _stCode = response.response?.statusCode, let _resURL = response.request?.url {
                print ("Response HTTP code: \(_stCode)")
                print("URL:   \(_resURL)")
            }
            
            if response.result.isSuccess, let responseJSON = response.result.value {
                self.handleSuccessResponse(response: response.response, data: responseJSON, dictionary: { (json) in
                    success(json)
                })
            } else {
                self.handleFailResponse(response: response.response, data: response.result.value, dictionary: { (json) in
                    
                    fail(json)
                })
            }
        }
    }
    
    private func handleSuccessResponse(response: HTTPURLResponse?, data: Any, dictionary: (_ json: JSON) -> ()) {
        let json: JSON = JSON(data)
        
        if self.printResponse {
        }
        
        dictionary(json)
    }
    
    private func handleFailResponse(response: HTTPURLResponse?, data: Any?, dictionary: (_ json: JSON) -> ()) {
        
        var responseDict: [String:AnyObject] = [String:AnyObject]()
        responseDict["error"] = "generalError" as AnyObject
        responseDict["code"] = "generalError" as AnyObject
        
        if (response != nil) {
            responseDict["responseCode"] = "\(response!.statusCode)" as AnyObject
        } else {
            responseDict["responseCode"] = "500" as AnyObject
        }
        
        if let data = data {
            let jsonData: JSON = JSON(data)
            if let code = jsonData["code"].string, let message = jsonData["msg"].string {
                responseDict["code"] = code.uppercased() as AnyObject
                responseDict["error"] = message as AnyObject
            }
        }
        
        if self.printResponse {
            print("response:   \(responseDict)")
        }
        
        let json = JSON(responseDict)
        
        dictionary(json)
    }
}


