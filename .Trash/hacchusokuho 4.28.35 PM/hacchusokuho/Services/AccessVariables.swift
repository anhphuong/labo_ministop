//
//  GetApiDomain.swift
//  hacchusokuho
//
//  Created by huy.thamgia on 10/12/18.
//  Copyright © 2018 huy.thamgia. All rights reserved.
//

import UIKit

class AccessVariables {
    //to get API link URL with key "domain api"
    static func getDomainApi(_ key: String) -> String? {
        return (Bundle.main.infoDictionary?[key] as? String)?
            .replacingOccurrences(of: "\\", with: "")
    }
    
    //to get API link URL with key "domain api"
    static func getApiPath(_ key: String) -> String {
        let pathApiDictionary = Bundle.main.infoDictionary?["path api"] as? [String: String]
        return pathApiDictionary?[key] ?? ""
    }
}
