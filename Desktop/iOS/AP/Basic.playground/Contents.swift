////: Playground - noun: a place where people can play
import UIKit
//// Type Aliases : Bi danh
////typealias SoNguyen = Int
////let a : SoNguyen = 13
////
//////Tuples: nhom nhieu gia tri hop thanh 1 gia tri don
////
////let info = ("Anh Phuong", 21)
////let (ten,tuoi) = info
////ten
////tuoi
////info.0
//////let (x,y) = (2,2)
//////if x == y {
//////    x
//////}
////
//////let lineBreaks = """
//////
//////This string starts with a line break.
//////It also ends with a line break.
//////
//////.
//////
//////"""
//////print(lineBreaks)
////
////let multilineString = """
////"These are the same."
////"""
////
////let softWrappedQuotation = """
////The White Rabbit put on his spectacles.  "Where shall I begin, \
////please your Majesty?" he asked.
////
////"Begin at the beginning," the King said gravely, "and go on \
////till you come to the end; then stop."
////"""
////// khong muon ngat dong la 1 phan cua gia tri chuoi
////let sparklingHeart = "\u{1F496}"
//////print(softWrappedQuotation)
////
////
////let threeDoubleQuotationMarks = """
////Escaping the first quotation mark \"""
////Escaping all three quotation marks \"\"\"
////"""
//////print(threeDoubleQuotationMarks)
////
////
////let goodStart = """
////one
////two
////
////"""
////let end = """
////three
////"""
//////print(goodStart + end)
////
////let precomposed: Character = "\u{D55C}"
////
////let greeting = "Guten Tag!"
////let index = greeting.index(greeting.startIndex, offsetBy: 7)
////greeting[index]
////
//
//var welcome = "hello"
//welcome.insert("!", at: welcome.endIndex)
//// welcome now equals "hello!"
//
////welcome.insert(contentsOf: " there", at: welcome.endIndex)
////let greeting = "Hello, world!"
////let index = greeting.firstIndex(of: ",") ?? greeting.endIndex
////let beginning = greeting[..<index]
//
//
//let romeoAndJuliet = [
//    "Act 1 Scene 1: Verona, A public place",
//    "Act 1 Scene 2: Capulet's mansion",
//    "Act 1 Scene 3: A room in Capulet's mansion",
//    "Act 1 Scene 4: A street outside Capulet's mansion",
//    "Act 1 Scene 5: The Great Hall in Capulet's mansion",
//    "Act 2 Scene 1: Outside Capulet's mansion",
//    "Act 2 Scene 2: Capulet's orchard",
//    "Act 2 Scene 3: Outside Friar Lawrence's cell",
//    "Act 2 Scene 4: A street in Verona",
//    "Act 2 Scene 5: Capulet's mansion",
//    "Act 2 Scene 6: Friar Lawrence's cell"
//]
//var act1SceneCount = 0
//for scene in romeoAndJuliet {
//    if scene.hasPrefix("Act 1 ") {
//        act1SceneCount += 1
//    }
//}
//
//
////var airports = ["YYZ": "Toronto Pearson", "DUB": "Dublin"]
////airports["LHR"] = "London"
////airports
////if let removedValue = airports.removeValue(forKey: "DUB") {
////    print("The removed airport's name is \(removedValue).")
////} else {
////    print("The airports dictionary does not contain a value for DUB.")
////}
//
//
//// Lap
////Buoc nhay
////let minuteInterval = 5
////let minutes = 60
////for tickMark in stride(from: 0, to: minutes, by: minuteInterval) {
////    // render the tick mark every 5 minutes (0, 5, 10, 15 ... 45, 50, 55)
////}
//
//func getInfor(name : String, age : Int){
//    print("Ten : " + name)
//}
//getInfor(name: "AFS", age: 18)
//let x = {(a: Int, b: Int) -> Int in
//    a+b
//}

// ARC
class User {
    var subscriptions: [CarrierSubscription] = []
    var name: String
    private(set) var phones: [Phone] = []
    func add(phone: Phone) {
        phones.append(phone)
        phone.owner = self
    }

    init(name: String) {
        self.name = name
        print("User \(name) is initialized")
    }
    deinit {
        print("User \(name) is being deallocated")
    }
    
}
class Phone {
    let model: String
    var owner: User?
    init(model: String) {
        self.model = model
        print("Phone \(model) is initialized")
    }
    deinit {
        print("Phone \(model) is being deallocated")
    }
    var carrierSubscription: CarrierSubscription?
    
    func provision(carrierSubscription: CarrierSubscription) {
        
        self.carrierSubscription = carrierSubscription
    }
    func decommission() {
        self.carrierSubscription = nil
    }
}
class CarrierSubscription {
    let name: String
    let countryCode: String
    let number: String
    let user: User
    init(name: String, countryCode: String, number: String, user: User) {
        self.name = name
        self.countryCode = countryCode
        self.number = number
        self.user = user
        user.subscriptions.append(self)
        print("CarrierSubscription \(name) is initialized")
    }
    deinit {
        print("CarrierSubscription \(name) is being deallocated")
    }
}
do {
    unowned let user1 = User(name: "John")
    let iPhone = Phone(model: "iPhone 6s Plus")
    user1.add(phone: iPhone)
    let subscription1 = CarrierSubscription(name: "TelBel", countryCode: "0032", number: "31415926", user: user1)
    iPhone.provision(carrierSubscription: subscription1)
    
}
//do {
//    let user1 = User(name: "John")
//    let iPhone = Phone(model: "iPhone 6s Plus")
//    user1.add(phone: iPhone)
//}

//let user1 = User(name: "Anh Phuong")
//do
//{
//    let user1 = User(name: "John")
//}
